package ru.tsc.ichaplygina.taskmanager.command.domain;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.tsc.ichaplygina.taskmanager.api.service.IProjectService;
import ru.tsc.ichaplygina.taskmanager.api.service.ITaskService;
import ru.tsc.ichaplygina.taskmanager.api.service.IUserService;
import ru.tsc.ichaplygina.taskmanager.command.AbstractCommand;
import ru.tsc.ichaplygina.taskmanager.dto.Domain;
import ru.tsc.ichaplygina.taskmanager.exception.other.ServiceLocatorNotFoundException;

import java.util.Optional;

public abstract class AbstractDomainCommand extends AbstractCommand {

    @NotNull
    protected static final String FILE_BINARY = "data.bin";

    @NotNull
    protected static final String FILE_BASE64 = "data.b64";

    @NotNull
    protected static final String FILE_FASTERXML_JSON = "data.fasterxml.json";

    @NotNull
    protected static final String FILE_FASTERXML_XML = "data.fasterxml.xml";

    @NotNull
    protected static final String FILE_FASTERXML_YAML = "data.fasterxml.yaml";

    @NotNull
    protected static final String FILE_JAXB_JSON = "data.jaxb.json";

    @NotNull
    protected static final String FILE_JAXB_XML = "data.jaxb.xml";

    @NotNull
    protected static final String JAVAX_XML_BIND_CONTEXT_FACTORY = "javax.xml.bind.context.factory";

    @NotNull
    protected static final String ORG_ECLIPSE_PERSISTENCE_JAXB_JAXBCONTEXT_FACTORY = "org.eclipse.persistence.jaxb.JAXBContextFactory";

    @NotNull
    protected static final String APPLICATION_JSON = "application/json";

    {
        setNeedAuthorization(true);
    }

    @Nullable
    public String getArgument() {
        return null;
    }

    @NotNull
    protected IProjectService getProjectService() {
        if (!Optional.ofNullable(serviceLocator).isPresent()) throw new ServiceLocatorNotFoundException();
        return serviceLocator.getProjectService();
    }

    @NotNull
    protected ITaskService getTaskService() {
        if (!Optional.ofNullable(serviceLocator).isPresent()) throw new ServiceLocatorNotFoundException();
        return serviceLocator.getTaskService();
    }

    @NotNull
    protected IUserService getUserService() {
        if (!Optional.ofNullable(serviceLocator).isPresent()) throw new ServiceLocatorNotFoundException();
        return serviceLocator.getUserService();
    }

    @NotNull
    protected Domain getDomain() {
        @NotNull final Domain domain = new Domain();
        domain.setProjects(getProjectService().findAll());
        domain.setTasks(getTaskService().findAll());
        domain.setUsers(getUserService().findAll());
        return domain;
    }

    protected void setDomain(@Nullable final Domain domain) {
        if (domain == null) return;
        getProjectService().clear();
        getProjectService().addAll(domain.getProjects());
        getTaskService().clear();
        getTaskService().addAll(domain.getTasks());
        getUserService().clear();
        getUserService().addAll(domain.getUsers());
    }

    protected void logoutAfterLoad() {
        System.out.println("Load completed.");
        System.out.println("Logging out.");
        getAuthService().logout();
        System.out.println("Logged out. Please log in");
        getCommandService().getCommands().get("login").execute();
    }

}
