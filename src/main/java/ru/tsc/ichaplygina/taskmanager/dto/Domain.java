package ru.tsc.ichaplygina.taskmanager.dto;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.dataformat.xml.annotation.JacksonXmlElementWrapper;
import com.fasterxml.jackson.dataformat.xml.annotation.JacksonXmlRootElement;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.jetbrains.annotations.Nullable;
import ru.tsc.ichaplygina.taskmanager.model.Project;
import ru.tsc.ichaplygina.taskmanager.model.Task;
import ru.tsc.ichaplygina.taskmanager.model.User;

import javax.xml.bind.annotation.*;
import java.io.Serializable;
import java.util.List;

@Getter
@Setter
@NoArgsConstructor
@XmlRootElement
@XmlType(name = "domain")
@XmlAccessorType(XmlAccessType.FIELD)
@JacksonXmlRootElement(localName = "domain")
public class Domain implements Serializable {

    @Nullable
    @XmlElement(name = "project")
    @JsonProperty("project")
    @XmlElementWrapper(name = "projects")
    @JacksonXmlElementWrapper(localName = "projects")
    private List<Project> projects;

    @Nullable
    @XmlElement(name = "task")
    @JsonProperty("task")
    @XmlElementWrapper(name = "tasks")
    @JacksonXmlElementWrapper(localName = "tasks")
    private List<Task> tasks;

    @Nullable
    @XmlElement(name = "user")
    @JsonProperty("user")
    @XmlElementWrapper(name = "users")
    @JacksonXmlElementWrapper(localName = "users")
    @JsonIgnoreProperties(value = {"fullName"})
    private List<User> users;

}
